package se.erikwiberg.main

object Start {
  def main(args: Array[String]): Unit = {

    val users = userArray

    logBeforeAndAfterCode("Print all users") {
      users foreach println
    }
	  
    logBeforeAndAfterCode("Total user age") {
    	val totalUserAge = (users foldLeft 0) { (age,usr) => age + usr.age }
    	println(totalUserAge)
    }
    
    logBeforeAndAfterCode("Say hello only to Ahmad 30") {
    	sayHelloOnlyToAhmadAge30(users)
    }
    
    logBeforeAndAfterCode("Say goodbye to first and fourth user") {
      sayGoodByeToFirstAndFourthUser(users toList)
    }
    
  }

  def userArray = {
    val names = "erik,kajsa,ahmad,nicole,emma" split ","
    val ages = Array.range(28, 28 + names.length)
    // convert list of (name,age) tuples into list of users
    names zip ages map { case (name, age) => User(name, age) }
  }
  
  
  // passing code into functions
  // emit type for argument
  def logBeforeAndAfterCode(codeBlockName: String)(code: => Any) = {
    println(String.format("\n--Before excecuting '%s'--", codeBlockName))
    
    code
    
    println(String.format("--After excecuting '%s'--", codeBlockName))
  }
  
  // pattern-matching for case classes 
  def sayHelloOnlyToAhmadAge30(users: Array[User]): Unit = {
    for(user <- users) {
    	user match {
    	  case User("ahmad",30) => println("Hello, Ahmad!")
    	  case User(_,_) => println("Sorry, you don't get a he.. greeting!")
    	}      
    }
  }
  
  // pattern-matching for Lists
  def sayGoodByeToFirstAndFourthUser(users: List[User]): Unit = {
    users match {
      case first :: _ :: _ :: fourth :: _ => { println(String.format("%s and %s will have to leave. Goodbye!", first.name, fourth.name)) }
      case _ => println("You can stay")
    }
  }
}

// case: provides getters, an apply (for use without the 'new' keyword)
case class User(name: String, age: Int)
